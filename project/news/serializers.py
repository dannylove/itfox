from rest_framework import serializers

from project.news.models import News, User


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        extra_kwargs = {
            'id': {'read_only': True},
            'password': {'write_only': True}
        }
        fields = ['id', 'username', 'password', 'email', 'first_name', 'last_name']


class NewsSerializer(serializers.ModelSerializer):

    class Meta:
        model = News
        fields = '__all__'
