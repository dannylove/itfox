from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    pass


class News(models.Model):
    title = models.CharField(max_length=200, unique=True, null=False, verbose_name='Заголовок новости')
    text = models.TextField(verbose_name='Текст новости')
    create_datetime = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания новости')
