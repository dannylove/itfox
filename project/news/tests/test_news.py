from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITransactionTestCase

from project.news.models import News, User
from project.news.serializers import NewsSerializer


class NewsCreateViewTests(APITransactionTestCase):
    """
    Проверка создания новости
    """

    def test_create_news_from_admin(self):
        """
        Создание новости администратором
        """
        self.superuser = User.objects.create_superuser(
            username='test_superuser',
            email='test@example.com',
            password='test_password',
        )

        self.client.force_authenticate(user=self.superuser)

        response = self.client.post(
            reverse('news:news-create'),
            data = {
                'title': 'test_news',
                'text': 'test_text'
            }
        )
        news_id = response.data['id']
        news = News.objects.get(id=news_id)
        serializer = NewsSerializer(news)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_news_from_user(self):
        """
        Создание новости пользователем
        """
        self.user = User.objects.create_user(
            username='test_user',
            email='test@example.com',
            password='test_password',
        )

        self.client.force_authenticate(user=self.user)

        response = self.client.post(
            reverse('news:news-create'),
            data = {
                'title': 'test_news',
                'text': 'test_text'
            }
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class NewsListViewTests(APITransactionTestCase):
    """
    Проверка получения списка новостей
    """

    def test_get_all_news_from_admin(self):
        """
        Получение списка новостей администратором
        """
        self.superuser = User.objects.create_superuser(
            username='test_superuser',
            email='test@example.com',
            password='test_password',
        )
        self.news = News.objects.create(
            title='test_news',
            text='test_text'
        )

        self.client.force_authenticate(user=self.superuser)

        response = self.client.get(reverse('news:news-list'))
        news = News.objects.all()
        serializer = NewsSerializer(news, many=True)
        self.assertEqual(response.data['results'], serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_all_news_from_user(self):
        """
        Получение списка новостей пользователем
        """
        self.user = User.objects.create_user(
            username='test_user',
            email='test@example.com',
            password='test_password',
        )
        self.news = News.objects.create(
            title='test_news',
            text='test_text'
        )

        self.client.force_authenticate(user=self.user)

        response = self.client.get(reverse('news:news-list'))
        news = News.objects.all()
        serializer = NewsSerializer(news, many=True)
        self.assertEqual(response.data['results'], serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class NewsDetailViewTests(APITransactionTestCase):
    """
    Проверка получения, обновления и удаления новости
    """

    def test_get_news_from_admin(self):
        """
        Получение новости администратором
        """
        self.superuser = User.objects.create_superuser(
            username='test_superuser',
            email='test@example.com',
            password='test_password',
        )
        self.news = News.objects.create(
            title='test_news',
            text='test_text'
        )

        self.client.force_authenticate(user=self.superuser)

        response = self.client.get(
            reverse(
                'news:news-detail',
                kwargs={'pk': self.news.id}
            )
        )
        news = News.objects.get(id=self.news.id)
        serializer = NewsSerializer(news)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_news_from_user(self):
        """
        Получение новости пользователем
        """
        self.user = User.objects.create_user(
            username='test_user',
            email='test@example.com',
            password='test_password',
        )
        self.news = News.objects.create(
            title='test_news',
            text='test_text'
        )

        self.client.force_authenticate(user=self.user)

        response = self.client.get(
            reverse(
                'news:news-detail',
                kwargs={'pk': self.news.id}
            )
        )
        news = News.objects.get(id=self.news.id)
        serializer = NewsSerializer(news)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_news_from_admin(self):
        """
        Обновление новости администратором
        """
        self.superuser = User.objects.create_superuser(
            username='test_superuser',
            email='test@example.com',
            password='test_password',
        )
        self.news = News.objects.create(
            title='test_news',
            text='test_text'
        )

        self.client.force_authenticate(user=self.superuser)

        response = self.client.patch(
            reverse('news:news-detail', kwargs={'pk': self.news.id}),
            data={'title': 'updated_test_news'}
        )
        news = News.objects.get(id=self.news.id)
        serializer = NewsSerializer(news)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_news_from_user(self):
        """
        Обновление новости пользователем
        """
        self.user = User.objects.create_user(
            username='test_user',
            email='test@example.com',
            password='test_password',
        )
        self.news = News.objects.create(
            title='test_news',
            text='test_text'
        )

        self.client.force_authenticate(user=self.user)

        response = self.client.patch(
            reverse('news:news-detail', kwargs={'pk': self.news.id}),
            data={'title': 'updated_test_news'}
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete_news_from_admin(self):
        """
        Удаление новости администратором
        """
        self.superuser = User.objects.create_superuser(
            username='test_superuser',
            email='test@example.com',
            password='test_password',
        )
        self.news = News.objects.create(
            title='test_news',
            text='test_text'
        )

        self.client.force_authenticate(user=self.superuser)

        response = self.client.delete(
            reverse(
                'news:news-detail',
                kwargs={'pk': self.news.id}
            )
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_news_from_user(self):
        """
        Удаление новости пользователем
        """
        self.user = User.objects.create_user(
            username='test_user',
            email='test@example.com',
            password='test_password',
        )
        self.news = News.objects.create(
            title='test_news',
            text='test_text'
        )

        self.client.force_authenticate(user=self.user)

        response = self.client.delete(
            reverse(
                'news:news-detail',
                kwargs={'pk': self.news.id}
            )
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
