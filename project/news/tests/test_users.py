from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITransactionTestCase

from project.news.models import User
from project.news.serializers import UserSerializer


class UserCreateViewTests(APITransactionTestCase):
    """
    Проверка создания пользователя
    """
    def test_create_user(self):
        """
        Создание нового пользователя
        """
        response = self.client.post(
            reverse('news:user-create'),
            data = {
                'username': 'test_user',
                'password': 'test_password',
                'email': 'test@example.com',
                'first_name': 'test',
                'last_name': 'user'
            },
        )
        user_id = response.data['id']
        user = User.objects.get(id=user_id)
        serializer = UserSerializer(user)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class UserListViewTests(APITransactionTestCase):
    """
    Проверка получения списка пользователей
    """

    def test_get_all_users_from_admin(self):
        """
        Получение списка пользователей администратором
        """
        self.superuser = User.objects.create_superuser(
            username='test_superuser',
            email='test@example.com',
            password='test_password',
        )

        self.client.force_authenticate(user=self.superuser)

        response = self.client.get(reverse('news:user-list'))
        users = User.objects.all()
        serializer = UserSerializer(users, many=True)
        self.assertEqual(response.data['results'], serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_all_users_from_user(self):
        """
        Получение списка пользователей пользователем
        """
        self.user = User.objects.create_user(
            username='test_user',
            email='test@example.com',
            password='test_password',
        )

        self.client.force_authenticate(user=self.user)

        response = self.client.get(reverse('news:user-list'))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class UserDetailViewTests(APITransactionTestCase):
    """
    Проверка получения, обновления и удаления пользователя
    """

    def test_get_self_user(self):
        """
        Получение текущего пользователя
        """
        self.user = User.objects.create_user(
            username='test_user',
            email='test@example.com',
            password='test_password',
        )

        self.client.force_authenticate(user=self.user)

        response = self.client.get(reverse('news:user-detail'))
        user = User.objects.get(id=self.user.id)
        serializer = UserSerializer(user)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_self_user(self):
        """
        Обновление текущего пользователя
        """
        self.user = User.objects.create_user(
            username='test_user',
            email='test@example.com',
            password='test_password',
        )

        self.client.force_authenticate(user=self.user)

        response = self.client.patch(
            reverse('news:user-detail'),
            data = {'username': 'updated_user'},
            format='json'
        )
        user = User.objects.get(id=self.user.id)
        serializer = UserSerializer(user)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_self_user(self):
        """
        Удаление текущего пользователя
        """
        self.user = User.objects.create_user(
            username='test_user',
            email='test@example.com',
            password='test_password',
        )

        self.client.force_authenticate(user=self.user)

        response = self.client.delete(reverse('news:user-detail'))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
