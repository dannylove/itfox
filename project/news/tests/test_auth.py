from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITransactionTestCase

from project.news.models import User


class UserAuthTests(APITransactionTestCase):
    """
    Проверка аутентификации пользователя
    """

    def test_auth_user(self):
        """
        Аутентификация пользователя
        """
        self.username='user'
        self.email='test@example.com'
        self.password='password'

        self.user = User.objects.create_user(
            username=self.username,
            email=self.email,
            password=self.password,
        )

        response = self.client.post(
            reverse('token_obtain_pair'),
            data = {
                'username': self.username,
                'password': self.password
            },
            format='json',
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_refresh_user_token(self):
        """
        Обновление токена пользователя
        """
        self.username='test_user'
        self.email='test@example.com'
        self.password='test_password'

        self.user = User.objects.create_user(
            username=self.username,
            email=self.email,
            password=self.password,
        )

        response = self.client.post(
            reverse('token_obtain_pair'),
            data = {
                'username': self.username,
                'password': self.password
            },
            format='json',
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.client.post(
            reverse('token_refresh'),
            data={'refresh': response.data['refresh']},
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_verify_user_token(self):
        """
        Проверка действительности токена пользователя
        """
        self.username='test_user'
        self.email='test@example.com'
        self.password='test_password'

        self.user = User.objects.create_user(
            username=self.username,
            email=self.email,
            password=self.password,
        )

        response = self.client.post(
            reverse('token_obtain_pair'),
            data = {
                'username': self.username,
                'password': self.password
            },
            format='json',
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.client.post(
            reverse('token_verify'),
            data={'token': response.data['access']},
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
