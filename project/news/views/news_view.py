from drf_yasg.utils import swagger_auto_schema
from rest_framework import generics
from rest_framework.permissions import IsAdminUser, IsAuthenticated

from project.news.models import News
from project.news.permissions import IsAdminOrReadOnly
from project.news.serializers import NewsSerializer


class NewsCreateView(generics.CreateAPIView):
    serializer_class = NewsSerializer
    permission_classes = (IsAdminUser, )

    @swagger_auto_schema(
        operation_description=(
            'Добавление новости.\n \
            Доступ имеет только Администратор'
        ),
        operation_summary='Добавление новости',
    )
    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class NewsListView(generics.ListAPIView):
    serializer_class = NewsSerializer
    queryset = News.objects.all()
    permission_classes = (IsAuthenticated, )

    @swagger_auto_schema(
        operation_description='Получение списка всех новостей',
        operation_summary='Получение новостей',
    )
    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class NewsDetailView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = NewsSerializer
    queryset = News.objects.all()
    permission_classes = (IsAdminOrReadOnly, )

    @swagger_auto_schema(
        operation_description='Получение новости по ID',
        operation_summary='Получение новости',
    )
    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    @swagger_auto_schema(
        operation_description=(
            'Обновление новости по ID.\n \
            Доступ имеет только Администратор'
        ),
        operation_summary='Обновление новости',
    )
    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)
    
    @swagger_auto_schema(
        operation_description=(
            'Частичное обновление новости по ID.\n \
            Доступ имеет только Администратор'
        ),
        operation_summary='Частичное обновление новости',
    )
    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)
    
    @swagger_auto_schema(
        operation_description=(
            'Удаление новости по ID.\n \
            Доступ имеет только Администратор'
        ),
        operation_summary='Удаление новости',
    )
    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)
