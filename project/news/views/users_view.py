from drf_yasg.utils import swagger_auto_schema
from rest_framework import generics
from rest_framework.permissions import AllowAny, IsAdminUser, IsAuthenticated

from project.news.models import User
from project.news.serializers import UserSerializer


class UserCreateView(generics.CreateAPIView):
    serializer_class = UserSerializer
    permission_classes = (AllowAny, )

    @swagger_auto_schema(
        operation_description='Добавление нового пользователя',
        operation_summary='Добавление пользователя',
    )
    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class UserListView(generics.ListAPIView):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = (IsAdminUser, )

    @swagger_auto_schema(
        operation_description=(
            'Получение списка всех пользователей.\n \
            Доступ имеет только Администратор'
        ),
        operation_summary='Получение пользователей',
    )
    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class UserDetailView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = (IsAuthenticated, )

    def get_object(self):
        return self.request.user

    @swagger_auto_schema(
        operation_description=(
            'Получение текущего пользователя.\n \
            Пользователь может получить только свою учетную запись'
        ),
        operation_summary='Получение текущего пользователя',
    )
    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    @swagger_auto_schema(
        operation_description=(
            'Обновление текущего пользователя.\n \
            Пользователь может обновить только свою учетную запись'
        ),
        operation_summary='Обновление текущего пользователя',
    )
    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    @swagger_auto_schema(
        operation_description=(
            'Частичное обновление текущего пользователя.\n \
            Пользователь может обновить только свою учетную запись'
        ),
        operation_summary='Частичное обновление текущего пользователя',
    )
    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    @swagger_auto_schema(
        operation_description=(
            'Удаление текущего пользователя.\n \
            Пользователь может удалить только свою учетную запись'
        ),
        operation_summary='Удаление текущего пользователя',
    )
    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)
