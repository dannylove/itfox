from django.contrib import admin

from project.news.models import News, User

admin.site.register(User)
admin.site.register(News)
