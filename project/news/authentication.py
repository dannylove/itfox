import jwt
from rest_framework import authentication, exceptions

from project import settings
from project.news.models import User


class CustomAuthentication(authentication.BaseAuthentication):
    """
    Кастомный класс аутентификации пользователя
    """

    def authenticate(self, request):
        token = request.META.get('HTTP_AUTHORIZATION')
        if not token:
            return None

        try:
            decoded_token = jwt.decode(
                token.split()[1],
                settings.SECRET_KEY,
                algorithms=settings.SIMPLE_JWT['ALGORITHM'],
            )
            user_id = decoded_token.get('user_id')
            user = User.objects.get(id=user_id)

        except User.DoesNotExist:
            raise exceptions.AuthenticationFailed('No such user')

        return (user, None)
