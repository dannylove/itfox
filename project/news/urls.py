from django.urls import path

from project.news.views.news_view import (NewsCreateView, NewsDetailView,
                                          NewsListView)
from project.news.views.users_view import (UserCreateView, UserDetailView,
                                           UserListView)

app_name = 'news'

urlpatterns = [
    path('users/create/', UserCreateView.as_view(), name='user-create'),
    path('users/', UserListView.as_view(), name='user-list'),
    path('users/current/', UserDetailView.as_view(), name='user-detail'),
    path('news/create/', NewsCreateView.as_view(), name='news-create'),
    path('news/', NewsListView.as_view(), name='news-list'),
    path('news/<int:pk>/', NewsDetailView.as_view(), name='news-detail'),
]
