# ItFox News API

Service for displaying news to authorized users

# Amazon EC2

[http://3.137.187.178/swagger](http://3.137.187.178/swagger)

Дефолтный администратор

```bash
username: admin
password: admin
```

# Локальное развёртывание проекта

Установка и активация виртуального окружения

```bash
python3 -m venv .venv
source .venv/bin/activate
```

Установка зависимостей

```bash
pip install -r requirements.txt
```

Установка переменных окружения (см. шаблон .env.example)

```bash
cd /polls/project
nano .env
```

Применение миграции к БД и создание администратора сервиса

```bash
python manage.py migrate
python manage.py createsuperuser
```

Запуск

```bash
python manage.py runserver
```

Swagger

[http://127.0.0.1:8000/swagger](http://127.0.0.1:8000/swagger)

Дефолтный администратор

```bash
username: admin
password: admin
```

# Docker

Развёртывание проекта при помощи Docker

Пример [docker-compose.yml](./docker-compose.yml)

```bash
docker-compose up
```

Swagger

[http://0.0.0.0:8000/swagger](http://0.0.0.0:8000/swagger)

Дефолтный администратор

```bash
username: admin
password: admin
```
