FROM python:3.7-alpine

WORKDIR /opt/app

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

COPY requirements.txt .
RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev \
    && pip install -r requirements.txt --no-cache-dir

COPY . .
